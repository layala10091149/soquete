import java.io.*;
import java.net.*;
import javax.swing.JTextArea;

public class ServidorHilo extends Thread {
     // declaración de instancias globales
       JTextArea display;
       Socket conexion;
       ServerSocket server;
       DataOutputStream output;
       DataInputStream input;
       double s1,s2,suma;
       int clienteNum;
    
    public ServidorHilo(JTextArea d, ServerSocket s, Socket c,int n)
    {
     display=d;
     server=s;
     conexion=c;
     clienteNum=n;
        System.out.println("se ha entrado al constructor del Hilo cliente: "+ n);
        
    }
    public void run(){
        try {
             display.append("se ha aceptado una conexion, para el cliente:"+ clienteNum+"\n");
             display.append("\nRecibiendo sumandos...\n");
             //creacion de las instancias para la transferencia de informacion
             input=new DataInputStream(conexion.getInputStream());
             output=new DataOutputStream(conexion.getOutputStream());
             
             display.append("\nRecibiendo el primer sumando...\n");
             s1=input.readDouble();
           
             display.append("\nRecibiendo el segundo sumando...\n");
             s2=input.readDouble();
             
             display.append("\nEnviando resultado...\n");
             suma= s1+s2;
             output.writeDouble(suma);
             display.append("\n Transmision terminada.\n Se cierra el socket del cliente: "+clienteNum+"\n");
            
       	conexion.close();
         }
      catch(IOException e){
                  e.printStackTrace();
                }

    }
}

